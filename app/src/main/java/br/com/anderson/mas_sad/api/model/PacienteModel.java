package br.com.anderson.mas_sad.api.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PacienteModel extends RealmObject {
    @PrimaryKey
    @SerializedName("pac_id")
    @Expose
    private int pac_id;

    @SerializedName("pac_nome")
    @Expose
    private String pac_nome;

    @SerializedName("pac_cpf")
    @Expose
    private String pac_cpf;

    @SerializedName("pac_endereco")
    @Expose
    private String pac_endereco;

    @SerializedName("pac_cep")
    @Expose
    private String pac_cep;

    @SerializedName("pac_email")
    @Expose
    private String pac_email;

    @SerializedName("pac_telefone")
    @Expose
    private String pac_telefone;

    @SerializedName("pac_cns")
    @Expose
    private String pac_cns;

    @SerializedName("pac_data_nascimento")
    @Expose
    private String pac_data_nascimento;

    @SerializedName("sexo")
    @Expose
    private SexoModel sexo;

    @SerializedName("sex_id")
    @Expose
    private int sex_id;

    @SerializedName("equipe")
    @Expose
    private EquipeModel equipe;

    @SerializedName("equ_id")
    @Expose
    private int equ_id;

    public PacienteModel() {

    }

    public PacienteModel(int pac_id, String pac_nome, String pac_cpf, String pac_endereco,
                         String pac_cep, String pac_email, String pac_telefone, String pac_cns,
                         String pac_data_nascimento, SexoModel sexo,
                         EquipeModel equipe, int sex_id, int equ_id) {
        this.pac_id = pac_id;
        this.pac_nome = pac_nome;
        this.pac_cpf = pac_cpf;
        this.pac_endereco = pac_endereco;
        this.pac_cep = pac_cep;
        this.pac_email = pac_email;
        this.pac_telefone = pac_telefone;
        this.pac_cns = pac_cns;
        this.pac_data_nascimento = pac_data_nascimento;
        this.sexo = sexo;
        this.equipe= equipe;
        this.sex_id= sex_id;
        this.equ_id= equ_id;
    }

    public int getPac_id() {
        return pac_id;
    }

    public void setPac_id(int pac_id) {
        this.pac_id = pac_id;
    }

    public String getPac_nome() {
        return pac_nome;
    }

    public void setPac_nome(String pac_nome) {
        this.pac_nome = pac_nome;
    }

    public String getPac_cpf() {
        return pac_cpf;
    }

    public void setPac_cpf(String pac_cpf) {
        this.pac_cpf = pac_cpf;
    }

    public String getPac_endereco() {
        return pac_endereco;
    }

    public void setPac_endereco(String pac_endereco) {
        this.pac_endereco = pac_endereco;
    }

    public String getPac_cep() {
        return pac_cep;
    }

    public void setPac_cep(String pac_cep) {
        this.pac_cep = pac_cep;
    }

    public String getPac_email() {
        return pac_email;
    }

    public void setPac_email(String pac_email) {
        this.pac_email = pac_email;
    }

    public String getPac_telefone() {
        return pac_telefone;
    }

    public void setPac_telefone(String pac_telefone) {
        this.pac_telefone = pac_telefone;
    }

    public String getPac_cns() {
        return pac_cns;
    }

    public void setPac_cns(String pac_cns) {
        this.pac_cns = pac_cns;
    }

    public String getPac_data_nascimento() {
        return pac_data_nascimento;
    }

    public void setPac_data_nascimento(String pac_data_nascimento) {
        this.pac_data_nascimento = pac_data_nascimento;
    }

    public SexoModel getSexo() {
        return sexo;
    }

    public void setSexo(SexoModel sexo) {
        this.sexo = sexo;
    }

    public EquipeModel getEquipe() {
        return equipe;
    }

    public void setEquipe(EquipeModel Equipe) {
        this.equipe = equipe;
    }

    public int getSex_id() {
        return sex_id;
    }

    public void setSex_id(int sex_id) {
        this.sex_id = sex_id;
    }

    public int getEqu_id() {
        return equ_id;
    }

    public void setEqu_id(int equ_id) {
        this.equ_id = equ_id;
    }

    @NonNull
    @Override
    public String toString() {
        return pac_nome;
    }
}
