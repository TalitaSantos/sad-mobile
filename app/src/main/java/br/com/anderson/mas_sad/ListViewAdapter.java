package br.com.anderson.mas_sad;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.List;
import br.com.anderson.mas_sad.api.model.AtendimentoModel;
import br.com.anderson.mas_sad.api.model.PacienteModel;
import br.com.anderson.mas_sad.api.model.SexoModel;
import io.realm.Realm;


public class ListViewAdapter extends BaseAdapter {

    private List<PacienteModel> pacienteModel;
    private Context context;
    private String sexoDescricao;
    private String dataUltimoAtendimento;
    private Boolean sync;

    public ListViewAdapter(Context context,List<PacienteModel> pacienteModel){
        this.context = context;
        this.pacienteModel = pacienteModel;
    }

    @Override
    public int getCount() {
        return pacienteModel.size();
    }

    @Override
    public Object getItem(int pos) {
        return pacienteModel.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if(view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.listview_pacientes,viewGroup,false);
        }

        TextView nomeTxt = view.findViewById(R.id.dataAtendimentoTextView);
        TextView idadeTxt = view.findViewById(R.id.profissionalAtendimentoTextView);
        TextView sexoTxt = view.findViewById(R.id.modalidadeAtendimentoTextView);
        TextView dataNascimentoTxt = view.findViewById(R.id.dataNascimentoPacienteTextView);

        final PacienteModel paciente = pacienteModel.get(position);

        nomeTxt.setText(paciente.getPac_nome());
        idadeTxt.setText(calculaIdade(paciente.getPac_data_nascimento()));
        sexoTxt.setText(sexoPaciente(paciente.getSex_id()));
        dataNascimentoTxt.setText(ultimoAtendimentoPaciente(paciente.getPac_id()));

        // Alterando Image
        if(!sincronizar(paciente.getPac_id())) {
            ImageView image = view.findViewById(R.id.barVertCatAte);
            image.setImageResource(R.drawable.rectangle_vertical_off);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, PacienteDetalhe.class);
                intent.putExtra("pac_id", paciente.getPac_id());
                context.startActivity(intent);
            }
        });

        return view;
    }

    private String calculaIdade(String data_nasicmento) {
        LocalDate l = LocalDate.parse(data_nasicmento);
        LocalDate now = LocalDate.now();
        Period diff = Period.between(l, now);
        return diff.getYears() + " anos";
    }

    private String sexoPaciente(final int sex_id) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                SexoModel s = realm.where(SexoModel.class).equalTo("sex_id", sex_id).findFirst();
                sexoDescricao = s.getSex_descricao();
            }
        });

        return sexoDescricao;
    }

    private String ultimoAtendimentoPaciente(final int pac_id) {
        dataUltimoAtendimento = "Sem atendimento";

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                List<AtendimentoModel> a = realm.where(AtendimentoModel.class).equalTo("pac_id", pac_id).findAll();

                if(a.size() != 0) {
                    Date data = a.get(0).getAte_data();
                    String dataFormatada = null;
                    for(int i = 0; i < a.size(); i++) {
                        if(data.before(a.get(i).getAte_data())) {
                            data = a.get(i).getAte_data();
                        }
//                        dataFormatada = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(data);
                        dataFormatada = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(data);
                    }
                    dataUltimoAtendimento = dataFormatada;
                }
            }
        });

        return dataUltimoAtendimento;
    }

    private Boolean sincronizar(final int pac_id) {
        sync = true;
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                List<AtendimentoModel> a = realm.where(AtendimentoModel.class).equalTo("pac_id", pac_id).findAll();

                if(a.size() != 0) {
                    for(int i = 0; i < a.size(); i++) {
                        if(!(a.get(i).getSync())) {
                            sync = false;
                        }
                    }
                }
            }
        });
        return sync;
    }
}