package br.com.anderson.mas_sad.api.service;

import java.util.List;

import br.com.anderson.mas_sad.api.model.CondicaoAvaliadaModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface CondicaoAvaliadaServices {
    @GET("condicao_avaliada")
    Call<List<CondicaoAvaliadaModel>> listCondicoesAvaliadas();
}
