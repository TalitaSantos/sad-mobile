package br.com.anderson.mas_sad.api.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProcedimentoModel extends RealmObject {
    @PrimaryKey
    @SerializedName("pce_id")
    @Expose
    private int pce_id;

    @SerializedName("pce_nome")
    @Expose
    private String pce_nome;

    @SerializedName("pce_codigo")
    @Expose
    private String pce_codigo;

    public ProcedimentoModel() {

    }

    public ProcedimentoModel(int pce_id, String pce_nome, String pce_codigo) {
        this.pce_id = pce_id;
        this.pce_nome = pce_nome;
        this.pce_codigo = pce_codigo;
    }

    public int getPce_id() {
        return pce_id;
    }

    public void setPce_id(int pce_id) {
        this.pce_id = pce_id;
    }

    public String getPce_nome() {
        return pce_nome;
    }

    public void setPce_nome(String pce_nome) {
        this.pce_nome = pce_nome;
    }

    public String getPce_codigo() {
        return pce_codigo;
    }

    public void setPce_codigo(String pce_codigo) {
        this.pce_codigo = pce_codigo;
    }

    @NonNull
    @Override
    public String toString() {
        return pce_nome;
    }
}
