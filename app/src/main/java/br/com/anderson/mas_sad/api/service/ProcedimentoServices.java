package br.com.anderson.mas_sad.api.service;

import java.util.List;

import br.com.anderson.mas_sad.api.model.ProcedimentoModel;
import br.com.anderson.mas_sad.api.model.TurnoModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ProcedimentoServices {
    @GET("procedimento")
    Call<List<ProcedimentoModel>> listProcedimentos();
}
