package br.com.anderson.mas_sad.utils;

import android.app.Activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;

import br.com.anderson.mas_sad.R;


public abstract class NavMenu extends AppCompatActivity {
    protected Drawer mDrawer = null;
    private AccountHeader headerResult = null;
    private IProfile profile;


    public static final int MENU_DASHBOARD = 1;
    public static final int MENU_PACIENTES = 2;
    public static final int MENU_SINCRONIZACAO = 3;
    public static final int MENU_SAIR = 4;

//    private PreferenceHelper preferenceHelper = new PreferenceHelper(this);
    protected void startMenu(Bundle savedInstanceState, Activity activity){

//        profile = new ProfileDrawerItem().withName(preferenceHelper.getNome()).withEmail("CNS - " + preferenceHelper.getCbo()).withIcon(getResources().getDrawable(R.drawable.img_unknown_avatar));

        buildHeader(false, savedInstanceState);

//        PrimaryDrawerItem p1 = new PrimaryDrawerItem().withIdentifier(3).withName(R.string.menu_home).withIcon(GoogleMaterial.Icon.gmd_assessment);

        mDrawer = new DrawerBuilder()
                .withActivity(activity)
                .withSavedInstance(savedInstanceState)
                .withDisplayBelowStatusBar(false)
                .withTranslucentStatusBar(false)
                .withActionBarDrawerToggle(true)
                .withAccountHeader(headerResult, false)
                .withDrawerLayout(R.layout.material_drawer_fits_not)
                .addDrawerItems(
                        new SecondaryDrawerItem().withName(R.string.menu_principal).withSelectable(false),
                        new PrimaryDrawerItem().withIdentifier(MENU_DASHBOARD).withName(R.string.menu_home).withIcon(R.drawable.ic_assessment),
                        new PrimaryDrawerItem().withIdentifier(MENU_PACIENTES).withName(R.string.menu_pacientes).withIcon(R.drawable.ic_home),
                        new DividerDrawerItem(),
                        new SecondaryDrawerItem().withName(R.string.menu_gerenciamento).withSelectable(false),
                        new PrimaryDrawerItem().withIdentifier(MENU_SINCRONIZACAO).withName(R.string.menu_sincronizar).withIcon(R.drawable.ic_sync).
                                withSelectedTextColor(getResources().getColor(R.color.colorPrimary)).withSelectedIconColor(getResources().getColor(R.color.colorPrimary)),
                        new PrimaryDrawerItem().withIdentifier(MENU_SAIR).withName(R.string.menu_sair).withIcon(R.drawable.ic_exit_to_app)
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        menuItemSelected((int) drawerItem.getIdentifier());
                        return true;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();

        mDrawer.setSelection(-1);
    }

    private void buildHeader(boolean compact, Bundle savedInstanceState) {
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.abstract_bkg)
                .withCompactStyle(compact)
//                .addProfiles(profile)
                .withTextColor(getResources().getColor((R.color.md_white_1000)))
                .withSavedInstance(savedInstanceState)
                .build();
    }

    protected abstract void menuItemSelected(final int activity);

}
