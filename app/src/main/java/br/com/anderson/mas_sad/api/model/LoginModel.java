package br.com.anderson.mas_sad.api.model;

import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("usu_usuario")
    private String usuario;

    @SerializedName("usu_senha")
    private String senha;

    @SerializedName("tipo")
    private int tipo;

    public LoginModel(String usuario, String senha, int tipo) {
        this.usuario = usuario;
        this.senha = senha;
        this.tipo = tipo;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
