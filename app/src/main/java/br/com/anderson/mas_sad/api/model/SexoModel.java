package br.com.anderson.mas_sad.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

//public class SexoModel extends RealmObject {
@RealmClass
public class SexoModel implements RealmModel {
    @PrimaryKey
    @SerializedName("sex_id")
    @Expose
    private int sex_id;

    @SerializedName("sex_descricao")
    @Expose
    private String sex_descricao;

    @SerializedName("sex_codigo")
    @Expose
    private String sex_codigo;

    public SexoModel() {

    }

    public SexoModel(int sex_id, String sex_descricao, String sex_codigo) {
        this.sex_id = sex_id;
        this.sex_descricao = sex_descricao;
        this.sex_codigo = sex_codigo;
    }

    public int getSex_id() {
        return sex_id;
    }

    public void setSex_id(int sex_id) {
        this.sex_id = sex_id;
    }

    public String getSex_descricao() {
        return sex_descricao;
    }

    public void setSex_descricao(String sex_descricao) {
        this.sex_descricao = sex_descricao;
    }

    public String getSex_codigo() {
        return sex_codigo;
    }

    public void setSex_codigo(String sex_codigo) {
        this.sex_codigo = sex_codigo;
    }
}
