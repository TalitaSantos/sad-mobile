package br.com.anderson.mas_sad.api.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ProfissionalModel extends RealmObject {
    @PrimaryKey
    @SerializedName("pro_id")
    @Expose
    private int pro_id;

    @SerializedName("pro_nome")
    @Expose
    private String pro_nome;

    @SerializedName("pro_cpf")
    @Expose
    private String pro_cpf;

    @SerializedName("pro_endereco")
    @Expose
    private String pro_endereco;

    @SerializedName("pro_cep")
    @Expose
    private String pro_cep;

    @SerializedName("pro_email")
    @Expose
    private String pro_email;

    @SerializedName("pro_telefone")
    @Expose
    private String pro_telefone;

    @SerializedName("pro_cns")
    @Expose
    private String pro_cns;

    @SerializedName("pro_cbo")
    @Expose
    private String pro_cbo;

    @SerializedName("pro_data_nascimento")
    @Expose
    private String pro_data_nascimento;

    @SerializedName("sexo")
    @Expose
    private SexoModel sexo;

    @SerializedName("sex_id")
    @Expose
    private int sex_id;

    @SerializedName("equipe")
    @Expose
    private EquipeModel equipe;

    @SerializedName("equ_id")
    @Expose
    private int equ_id;

    public ProfissionalModel() {

    }

    public ProfissionalModel(int pro_id, String pro_nome, String pro_cpf, String pro_endereco,
                             String pro_cep, String pro_email, String pro_telefone, String pro_cns,
                             String pro_cbo, String pro_data_nascimento, SexoModel sexo, int sex_id,
                             EquipeModel equipe, int equ_id) {
        this.pro_id = pro_id;
        this.pro_nome = pro_nome;
        this.pro_cpf = pro_cpf;
        this.pro_endereco = pro_endereco;
        this.pro_cep = pro_cep;
        this.pro_email = pro_email;
        this.pro_telefone = pro_telefone;
        this.pro_cns = pro_cns;
        this.pro_cbo = pro_cbo;
        this.pro_data_nascimento = pro_data_nascimento;
        this.sexo = sexo;
        this.sex_id = sex_id;
        this.equipe = equipe;
        this.equ_id = equ_id;
    }

    public int getPro_id() {
        return pro_id;
    }

    public void setPro_id(int pro_id) {
        this.pro_id = pro_id;
    }

    public String getPro_nome() {
        return pro_nome;
    }

    public void setPro_nome(String pro_nome) {
        this.pro_nome = pro_nome;
    }

    public String getPro_cpf() {
        return pro_cpf;
    }

    public void setPro_cpf(String pro_cpf) {
        this.pro_cpf = pro_cpf;
    }

    public String getPro_endereco() {
        return pro_endereco;
    }

    public void setPro_endereco(String pro_endereco) {
        this.pro_endereco = pro_endereco;
    }

    public String getPro_cep() {
        return pro_cep;
    }

    public void setPro_cep(String pro_cep) {
        this.pro_cep = pro_cep;
    }

    public String getPro_email() {
        return pro_email;
    }

    public void setPro_email(String pro_email) {
        this.pro_email = pro_email;
    }

    public String getPro_telefone() {
        return pro_telefone;
    }

    public void setPro_telefone(String pro_telefone) {
        this.pro_telefone = pro_telefone;
    }

    public String getPro_cns() {
        return pro_cns;
    }

    public void setPro_cns(String pro_cns) {
        this.pro_cns = pro_cns;
    }

    public String getPro_cbo() {
        return pro_cbo;
    }

    public void setPro_cbo(String pro_cbo) {
        this.pro_cbo = pro_cbo;
    }

    public String getPro_data_nascimento() {
        return pro_data_nascimento;
    }

    public void setPro_data_nascimento(String pro_data_nascimento) {
        this.pro_data_nascimento = pro_data_nascimento;
    }

    public SexoModel getSexo() {
        return sexo;
    }

    public void setSexo(SexoModel sexo) {
        this.sexo = sexo;
    }

    public int getSex_id() {
        return sex_id;
    }

    public void setSex_id(int sex_id) {
        this.sex_id = sex_id;
    }

    public EquipeModel getEquipe() {
        return equipe;
    }

    public void setEquipe(EquipeModel equipe) {
        this.equipe = equipe;
    }

    public int getEqu_id() {
        return equ_id;
    }

    public void setEqu_id(int equ_id) {
        this.equ_id = equ_id;
    }

    @NonNull
    @Override
    public String toString() {
        return pro_nome;
    }

}
