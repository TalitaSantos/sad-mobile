package br.com.anderson.mas_sad.api.service;

import java.util.List;
import br.com.anderson.mas_sad.api.model.LocalAtendimentoModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface LocalAtendimentoServices {
    @GET("local_atendimento")
    Call<List<LocalAtendimentoModel>> listLocalAtendimentos();
}
