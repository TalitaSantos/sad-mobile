package br.com.anderson.mas_sad.api.service;

import java.util.List;

import br.com.anderson.mas_sad.api.model.TipoAtendimentoModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface TipoAtendimentoServices {
    @GET("tipo_atendimento")
    Call<List<TipoAtendimentoModel>> listTipoAtendimentos();
}
