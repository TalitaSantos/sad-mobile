package br.com.anderson.mas_sad.api.service;

import java.util.List;

import br.com.anderson.mas_sad.api.model.CondutaDesfechoModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface CondutaDesfechoServices {
    @GET("conduta_desfecho")
    Call<List<CondutaDesfechoModel>> listCondutaDesfechos();
}
