package br.com.anderson.mas_sad;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.anderson.mas_sad.api.model.AtendimentoModel;
import br.com.anderson.mas_sad.api.model.ModalidadeModel;
import br.com.anderson.mas_sad.api.model.ProfissionalModel;
import io.realm.Realm;


public class ListViewAdapterAtendimento extends BaseAdapter {

    private List<AtendimentoModel> atendimentoModel;
    private Context context;
    private String profissionalNome;
    private String modalidadeNome;
//    private String dataUltimoAtendimento;

    public ListViewAdapterAtendimento(Context context, List<AtendimentoModel> atendimentoModel){
        this.context = context;
        this.atendimentoModel = atendimentoModel;
    }

    @Override
    public int getCount() {
        return atendimentoModel.size();
    }

    @Override
    public Object getItem(int pos) {
        return atendimentoModel.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        if(view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.listview_atendimentos,viewGroup,false);
        }

        TextView dataTxt = view.findViewById(R.id.dataAtendimentoTextView);
        TextView profissionalTxt = view.findViewById(R.id.profissionalAtendimentoTextView);
        TextView modalidadeTxt = view.findViewById(R.id.modalidadeAtendimentoTextView);

        final AtendimentoModel atendimento = atendimentoModel.get(position);

        dataTxt.setText(formatarData(atendimento.getAte_data()));
        profissionalTxt.setText(profissional(atendimento.getPro_id()));
        modalidadeTxt.setText(modalidade(atendimento.getMod_id()));

        // Alterando Image
        if(!atendimento.getSync()) {
            ImageView image = view.findViewById(R.id.barVertCatAte);
            image.setImageResource(R.drawable.rectangle_vertical_off);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AtendimentoDetalhe.class);
                intent.putExtra("ate_id", atendimento.getAte_id());
                context.startActivity(intent);
            }
        });

        return view;
    }

    private String profissional(final int pro_id) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                ProfissionalModel p = realm.where(ProfissionalModel.class).equalTo("pro_id", pro_id).findFirst();
                profissionalNome = p.getPro_nome();
            }
        });

        return profissionalNome;
    }

    private String modalidade(final int mod_id) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                ModalidadeModel m = realm.where(ModalidadeModel.class).equalTo("mod_id", mod_id).findFirst();
                modalidadeNome = m.getMod_descricao();
            }
        });

        return modalidadeNome;
    }

    private String formatarData(Date data) {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(data);
    }

    /*
    private String ultimoAtendimentoPaciente(final int pac_id) {
        dataUltimoAtendimento = "Sem atendimento";

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                List<AtendimentoModel> a = realm.where(AtendimentoModel.class).equalTo("pac_id", pac_id).findAll();

                if(a.size() != 0) {
                    Date data = a.get(0).getAte_data();
                    String dataFormatada = null;
                    for(int i = 0; i < a.size(); i++) {
                        if(data.before(a.get(i).getAte_data())) {
                            data = a.get(i).getAte_data();
                        }
                        //dataFormatada = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(data);
                        dataFormatada = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(data);
                    }
                    dataUltimoAtendimento = dataFormatada;
                }
            }
        });

        return dataUltimoAtendimento;
    }
    */
}