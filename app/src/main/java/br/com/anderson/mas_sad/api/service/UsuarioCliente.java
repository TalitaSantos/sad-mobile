package br.com.anderson.mas_sad.api.service;

import br.com.anderson.mas_sad.api.model.LoginModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import br.com.anderson.mas_sad.api.model.Usuario;

public interface UsuarioCliente {
    @POST("loginModel")
    Call<Usuario> login(@Body LoginModel loginModel);
}
