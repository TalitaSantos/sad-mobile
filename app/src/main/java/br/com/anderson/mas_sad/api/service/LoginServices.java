package br.com.anderson.mas_sad.api.service;

import br.com.anderson.mas_sad.api.model.LoginModel;
import br.com.anderson.mas_sad.api.model.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface LoginServices {
    @POST("login")
    Call<Usuario> login(
            @Body LoginModel loginModel
//           @Header {"Content-Type: application/json"}
    );
}