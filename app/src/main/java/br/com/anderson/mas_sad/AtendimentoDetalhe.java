package br.com.anderson.mas_sad;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import br.com.anderson.mas_sad.api.model.AtendimentoModel;
import br.com.anderson.mas_sad.api.model.PacienteModel;
import br.com.anderson.mas_sad.api.model.ProfissionalModel;
import br.com.anderson.mas_sad.api.model.SexoModel;
import br.com.anderson.mas_sad.utils.NavMenu;
import io.realm.Realm;

public class AtendimentoDetalhe  extends NavMenu {

    private Toolbar toolbar;
    private TextView paciente;
    private TextView profissional;
    private TextView data;
    private TextView turno;
    private TextView local_atendimento;
    private TextView modalidade;
    private TextView tipo_atendimento;
    private TextView conduta_desfecho;
    private TextView cid10;
    private TextView ciap2;
    private TextView condicoes_avaliada;
    private TextView procedimento;
    private TextView outros_procedimentos;

    private PreferenceHelper preferenceHelper;
    private Sincronizar sincronizar = new Sincronizar(AtendimentoDetalhe.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atendimento_detalhe);

        preferenceHelper = new PreferenceHelper(this);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        int ate_id = (int) getIntent().getSerializableExtra("ate_id");
        AtendimentoModel atendimento = atendimentoDetalhe(ate_id);

        Log.e("asdfkljlajsf", "--------------------" + atendimento.getPaciente().getPac_nome());

        paciente = findViewById(R.id.pacienteId);
        profissional = findViewById(R.id.profissionalId);
        data = findViewById(R.id.dataAtendimentoId);
        turno = findViewById(R.id.turnoId);
        local_atendimento = findViewById(R.id.localAtendimentoId);
        modalidade = findViewById(R.id.modalidadeId);
        tipo_atendimento = findViewById(R.id.tipoAtendimentoId);
        conduta_desfecho = findViewById(R.id.condutaDesfechoId);
        cid10 = findViewById(R.id.cid10Id);
        ciap2 = findViewById(R.id.ciap2Id);
        condicoes_avaliada = findViewById(R.id.condicoesAvaliadaId);
        procedimento = findViewById(R.id.procedimentoId);
        outros_procedimentos = findViewById(R.id.outrosProcedimentosId);

        paciente.setText(atendimento.getPaciente().getPac_nome());
        profissional.setText(atendimento.getProfissional().getPro_nome());
        data.setText(atendimento.getAte_data().toString());
        turno.setText(atendimento.getTurno().getTur_descricao());
        local_atendimento.setText(atendimento.getLocal_atendimento().getLoc_nome());
        modalidade.setText((atendimento.getModalidade().getMod_descricao() != null) ? atendimento.getModalidade().getMod_descricao() : " - ");
        tipo_atendimento.setText(atendimento.getTipo_atendimento().getTat_nome());
        conduta_desfecho.setText(atendimento.getConduta_desfecho().getCde_descricao());
        cid10.setText((atendimento.getAte_cid10() != null) ? atendimento.getAte_cid10() : " - ");
        ciap2.setText((atendimento.getAte_ciap2() != null) ? atendimento.getAte_ciap2() : " - ");
        //condicoes_avaliada.setText();
        //procedimento.setText();
        //outros_procedimentos.setText();
        this.startMenu(savedInstanceState, this);
    }

    public AtendimentoModel atendimentoDetalhe(int ate_id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        AtendimentoModel atendimento = realm.where(AtendimentoModel.class).equalTo("ate_id", ate_id).findFirst();
        realm.commitTransaction();
        realm.close();
        return atendimento;
    }

    private String sexoPaciente(int sex_id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        SexoModel sexo = realm.where(SexoModel.class).equalTo("sex_id", sex_id).findFirst();
        realm.commitTransaction();
        realm.close();
        return sexo.getSex_descricao();
    }

    @Override
    public void onResume(){
        super.onResume();

        mDrawer.resetDrawerContent();
        mDrawer.setSelection(MENU_PACIENTES);
    }

    @Override
    public void onBackPressed() {
        if (super.mDrawer.isDrawerOpen()) {
            super.mDrawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                if(super.mDrawer.isDrawerOpen()){
                    super.mDrawer.closeDrawer();
                }else {
                    super.mDrawer.openDrawer();
                }
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void menuItemSelected(int activity) {
        switch(activity) {
            case (NavMenu.MENU_DASHBOARD):
                mDrawer.closeDrawer();
                break;
            case NavMenu.MENU_PACIENTES:
                mDrawer.closeDrawer();
                break;
            case NavMenu.MENU_SINCRONIZACAO:
                confirmSync();
                break;
            case NavMenu.MENU_SAIR:
                confirmExit();
                break;
            default:
                break;
        }
    }

    public void confirmExit(){
        new LovelyStandardDialog(this)
                .setTopColorRes(R.color.colorPrimary)
                .setTopTitle("Sair")
                .setTopTitleColor(this.getResources().getColor(R.color.md_white_1000))
                .setIcon(R.drawable.ic_exit_to_app_white)
                .setMessage("Deseja Realmente Sair?")
                .setMessageGravity(Gravity.CENTER)
                .setButtonsColor(getResources().getColor(R.color.md_white_1000))
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deslogarUsuario();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    public void confirmSync(){
        new LovelyStandardDialog(this)
                .setTopColorRes(R.color.colorPrimary)
                .setTopTitle("Sincronização")
                .setTopTitleColor(this.getResources().getColor(R.color.md_white_1000))
                .setIcon(R.drawable.ic_sync_black_white)
                .setMessage("Deseja sincronizar?")
                .setMessageGravity(Gravity.CENTER)
                .setButtonsColor(getResources().getColor(R.color.md_white_1000))
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sync();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    private void deslogarUsuario() {
        preferenceHelper.putIsLogin(false);
        preferenceHelper.putId(-1);
        preferenceHelper.putNome("");
        preferenceHelper.putCns("");
        preferenceHelper.putCbo("");
        preferenceHelper.putEmail("");
        preferenceHelper.putEquipe(-1);
        preferenceHelper.putEquipeNome("");
        preferenceHelper.putEquipeIne("");
        preferenceHelper.putUnidadeNome("");
        preferenceHelper.putUnidadeCnes("");
        preferenceHelper.putToken("");

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(AtendimentoModel.class);
        realm.delete(PacienteModel.class);
        realm.delete(ProfissionalModel.class);
        realm.commitTransaction();
        realm.close();

        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        this.finish();
    }

    private void sync(){
        Toast.makeText(this, "Sincronizando...!", Toast.LENGTH_SHORT).show();
        // Recupera o id da equipe armazenado no sharedpreference
        String equipe = Integer.toString(preferenceHelper.getEquipe());
        String token = preferenceHelper.getToken();
        sincronizar.atendimentoSalvar(equipe, token);
        // Armazena no banco de dados os pacientes, profissionais e atendimentos
        sincronizar.atendimentos(equipe, token);
        sincronizar.profissionais(equipe, token);
        sincronizar.pacientes(equipe, token, getSupportFragmentManager());
        // Armazena no banco de dados as tabelas de domínio
        sincronizar.localAtendimento();
        sincronizar.modalidade();
        sincronizar.tipoAtendimento();
        sincronizar.condicaoAvaliada();
        sincronizar.condutaDesfecho();
        sincronizar.procedimento();
        sincronizar.turno();
    }
}
