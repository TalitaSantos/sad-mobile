package br.com.anderson.mas_sad;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import br.com.anderson.mas_sad.api.model.AtendimentoModel;
import br.com.anderson.mas_sad.api.model.PacienteModel;
import br.com.anderson.mas_sad.api.model.ProfissionalModel;
import br.com.anderson.mas_sad.api.model.SexoModel;
import br.com.anderson.mas_sad.utils.NavMenu;
import io.realm.Realm;
import io.realm.RealmResults;

public class PacienteDetalhe extends NavMenu {

    private Toolbar toolbar;
    private TextView nome;
    private TextView data_nascimento;
    private TextView cpf;
    private TextView endereco;
    private TextView email;
    private TextView cns;
    private TextView sexo;
    private TextView telefone;

    private ListViewAdapterAtendimento adapter;
    private ListView mListView;
    private FloatingActionButton floatAtendimento;
    private PreferenceHelper preferenceHelper;
    private Sincronizar sincronizar = new Sincronizar(PacienteDetalhe.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paciente_detalhe);

        preferenceHelper = new PreferenceHelper(this);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_button);

        int pac_id = (int) getIntent().getSerializableExtra("pac_id");
        PacienteModel paciente = pacienteDetalhe(pac_id);

        nome = findViewById(R.id.pacienteId);
        data_nascimento = findViewById(R.id.profissionalId);
        cpf = findViewById(R.id.dataAtendimentoId);
        endereco = findViewById(R.id.turnoId);
        email = findViewById(R.id.localAtendimentoId);
        cns = findViewById(R.id.tipoAtendimentoId);
        sexo = findViewById(R.id.condutaDesfechoId);
        telefone = findViewById(R.id.modalidadeId);
        floatAtendimento = findViewById(R.id.floatAtendimento);

        nome.setText(paciente.getPac_nome());
        data_nascimento.setText(calculaIdade(paciente.getPac_data_nascimento()));
        cpf.setText(paciente.getPac_cpf());
        endereco.setText(paciente.getPac_endereco());
        email.setText(paciente.getPac_email());
        cns.setText(paciente.getPac_cns());
        if(paciente.getPac_telefone().isEmpty()){
            sexo.setText("--");
        }else{
            sexo.setText(sexoPaciente(paciente.getSex_id()));
        }
        if(paciente.getPac_telefone() == null || paciente.getPac_telefone().isEmpty()){
            telefone.setText("--");
        }else{
            telefone.setText(paciente.getPac_telefone());
        }

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<AtendimentoModel> atendimentos = realm.where(AtendimentoModel.class).equalTo("pac_id", pac_id).findAll();

        final List<AtendimentoModel> aList = new ArrayList<>();
        for (int i = 0; i < atendimentos.size(); i++) {
            AtendimentoModel a = new AtendimentoModel(
                    atendimentos.get(i).getAte_id(),
                    atendimentos.get(i).getAte_nome(),
                    atendimentos.get(i).getTurno(),
                    atendimentos.get(i).getTur_id(),
                    atendimentos.get(i).getPaciente(),
                    atendimentos.get(i).getPac_id(),
                    atendimentos.get(i).getLocal_atendimento(),
                    atendimentos.get(i).getLoc_id(),
                    atendimentos.get(i).getModalidade(),
                    atendimentos.get(i).getMod_id(),
                    atendimentos.get(i).getTipo_atendimento(),
                    atendimentos.get(i).getTat_id(),
                    atendimentos.get(i).getConduta_desfecho(),
                    atendimentos.get(i).getCde_id(),
                    atendimentos.get(i).getAte_cid10(),
                    atendimentos.get(i).getAte_ciap2(),
                    atendimentos.get(i).getProfissional(),
                    atendimentos.get(i).getPro_id(),
                    atendimentos.get(i).getAte_data(),
                    atendimentos.get(i).getSync()
            );
            aList.add(a);
        }

        populateListView(aList);

        floatAtendimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PacienteDetalhe.this, NewAtendimentoActivity.class);
                startActivity(intent);
                PacienteDetalhe.this.finish();
            }
        });

        this.startMenu(savedInstanceState, this);
        realm.commitTransaction();
        realm.close();
    }

    public PacienteModel pacienteDetalhe(int pac_id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        PacienteModel paciente = realm.where(PacienteModel.class).equalTo("pac_id", pac_id).findFirst();
        realm.commitTransaction();
        realm.close();
        return paciente;
    }

    private String calculaIdade(String data_nasicmento) {
        LocalDate l = LocalDate.parse(data_nasicmento);
        LocalDate now = LocalDate.now();
        Period diff = Period.between(l, now);
        return diff.getYears() + " anos";
    }

    private String sexoPaciente(int sex_id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        SexoModel sexo = realm.where(SexoModel.class).equalTo("sex_id", sex_id).findFirst();
        realm.commitTransaction();
        realm.close();
        return sexo.getSex_descricao();
    }

    private void populateListView(List<AtendimentoModel> atendimentoModelList) {
        mListView = findViewById(R.id.listViewAtendimentos);
        adapter = new ListViewAdapterAtendimento(PacienteDetalhe.this, atendimentoModelList);
        mListView.setAdapter(adapter);
    }

    @Override
    public void onResume(){
        super.onResume();

        mDrawer.resetDrawerContent();
        mDrawer.setSelection(MENU_PACIENTES);
    }

    @Override
    public void onBackPressed() {
        if (super.mDrawer.isDrawerOpen()) {
            super.mDrawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                if(super.mDrawer.isDrawerOpen()){
                    super.mDrawer.closeDrawer();
                }else {
                    super.mDrawer.openDrawer();
                }
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void menuItemSelected(int activity) {
        switch(activity) {
            case (NavMenu.MENU_DASHBOARD):
                mDrawer.closeDrawer();
                break;
            case NavMenu.MENU_PACIENTES:
                mDrawer.closeDrawer();
                break;
            case NavMenu.MENU_SINCRONIZACAO:
                confirmSync();
                break;
            case NavMenu.MENU_SAIR:
                confirmExit();
                break;
            default:
                break;
        }
    }

    public void confirmExit(){
        new LovelyStandardDialog(this)
                .setTopColorRes(R.color.colorPrimary)
                .setTopTitle("Sair")
                .setTopTitleColor(this.getResources().getColor(R.color.md_white_1000))
                .setIcon(R.drawable.ic_exit_to_app_white)
                .setMessage("Deseja Realmente Sair?")
                .setMessageGravity(Gravity.CENTER)
                .setButtonsColor(getResources().getColor(R.color.md_white_1000))
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deslogarUsuario();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    public void confirmSync(){
        new LovelyStandardDialog(this)
                .setTopColorRes(R.color.colorPrimary)
                .setTopTitle("Sincronização")
                .setTopTitleColor(this.getResources().getColor(R.color.md_white_1000))
                .setIcon(R.drawable.ic_sync_black_white)
                .setMessage("Deseja sincronizar?")
                .setMessageGravity(Gravity.CENTER)
                .setButtonsColor(getResources().getColor(R.color.md_white_1000))
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sync();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    private void deslogarUsuario() {
        preferenceHelper.putIsLogin(false);
        preferenceHelper.putId(-1);
        preferenceHelper.putNome("");
        preferenceHelper.putCns("");
        preferenceHelper.putCbo("");
        preferenceHelper.putEmail("");
        preferenceHelper.putEquipe(-1);
        preferenceHelper.putEquipeNome("");
        preferenceHelper.putEquipeIne("");
        preferenceHelper.putUnidadeNome("");
        preferenceHelper.putUnidadeCnes("");
        preferenceHelper.putToken("");

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(AtendimentoModel.class);
        realm.delete(PacienteModel.class);
        realm.delete(ProfissionalModel.class);
        realm.commitTransaction();
        realm.close();

        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        this.finish();
    }

    private void sync(){
        Toast.makeText(this, "Sincronizando...!", Toast.LENGTH_SHORT).show();
        // Recupera o id da equipe armazenado no sharedpreference
        String equipe = Integer.toString(preferenceHelper.getEquipe());
        String token = preferenceHelper.getToken();
        sincronizar.atendimentoSalvar(equipe, token);
        // Armazena no banco de dados os pacientes, profissionais e atendimentos
        sincronizar.atendimentos(equipe, token);
        sincronizar.profissionais(equipe, token);
        sincronizar.pacientes(equipe, token, getSupportFragmentManager());
        // Armazena no banco de dados as tabelas de domínio
        sincronizar.localAtendimento();
        sincronizar.modalidade();
        sincronizar.tipoAtendimento();
        sincronizar.condicaoAvaliada();
        sincronizar.condutaDesfecho();
        sincronizar.procedimento();
        sincronizar.turno();
    }
}
