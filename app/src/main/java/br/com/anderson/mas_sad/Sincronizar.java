package br.com.anderson.mas_sad;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.List;
import br.com.anderson.mas_sad.api.model.AtendimentoModel;
import br.com.anderson.mas_sad.api.model.CondicaoAvaliadaModel;
import br.com.anderson.mas_sad.api.model.CondutaDesfechoModel;
import br.com.anderson.mas_sad.api.model.LocalAtendimentoModel;
import br.com.anderson.mas_sad.api.model.ModalidadeModel;
import br.com.anderson.mas_sad.api.model.PacienteModel;
import br.com.anderson.mas_sad.api.model.ProcedimentoModel;
import br.com.anderson.mas_sad.api.model.ProfissionalModel;
import br.com.anderson.mas_sad.api.model.TipoAtendimentoModel;
import br.com.anderson.mas_sad.api.model.TurnoModel;
import br.com.anderson.mas_sad.api.service.AtendimentoServices;
import br.com.anderson.mas_sad.api.service.CondicaoAvaliadaServices;
import br.com.anderson.mas_sad.api.service.CondutaDesfechoServices;
import br.com.anderson.mas_sad.api.service.LocalAtendimentoServices;
import br.com.anderson.mas_sad.api.service.ModalidadeServices;
import br.com.anderson.mas_sad.api.service.PacienteServices;
import br.com.anderson.mas_sad.api.service.ProcedimentoServices;
import br.com.anderson.mas_sad.api.service.ProfissionalServices;
import br.com.anderson.mas_sad.api.service.TipoAtendimentoServices;
import br.com.anderson.mas_sad.api.service.TurnoServices;
import br.com.anderson.mas_sad.ui.ConfiguracaoFragment;
import br.com.anderson.mas_sad.ui.PacienteFragment;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sincronizar {

    private Context context;

    public Sincronizar(Context context) {
        this.context = context;

    }

    public void atendimentoSalvar(final String equipe, final String token) {

        List<AtendimentoModel> atendimentos = getAtendimentoDB();

        for (int i = 0; i < atendimentos.size(); i++) {
            final int atendimento_id = atendimentos.get(i).getAte_id();
            AtendimentoServices api = RetrofitClient.getRetrofitInstance().create(AtendimentoServices.class);

            AtendimentoModel data = new AtendimentoModel();
            data.setPac_id(atendimentos.get(i).getPac_id());
            data.setPro_id(atendimentos.get(i).getPro_id());
            data.setLoc_id(atendimentos.get(i).getLoc_id());
            data.setMod_id(atendimentos.get(i).getMod_id());
            data.setTat_id(atendimentos.get(i).getTat_id());
            data.setCde_id(atendimentos.get(i).getCde_id());
            data.setAte_cid10(atendimentos.get(i).getAte_cid10());
            data.setAte_ciap2(atendimentos.get(i).getAte_ciap2());
            data.setTur_id(atendimentos.get(i).getTur_id());
            data.setAte_data(atendimentos.get(i).getAte_data());

            Call<AtendimentoModel> call = api.atendimento(equipe, token, data);
            call.enqueue(new Callback<AtendimentoModel>() {
                @Override
                public void onResponse(Call<AtendimentoModel> call, Response<AtendimentoModel> response) {
                    deleteAtendimentoDB(atendimento_id);
                    //Toast.makeText(context, "Atendimentos baixado", Toast.LENGTH_LONG).show();
                }
                @Override
                public void onFailure(Call<AtendimentoModel> call, Throwable throwable) {
                    //Toast.makeText(context, "Falha ao baixar atendimentos", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    public void atendimentos(String equipe, String token) {
        AtendimentoServices api = RetrofitClient.getRetrofitInstance().create(AtendimentoServices.class);
        Call<List<AtendimentoModel>> call = api.listAtendimentos(equipe, token);
        call.enqueue(new Callback<List<AtendimentoModel>>() {
            @Override
            public void onResponse(Call<List<AtendimentoModel>> call, Response<List<AtendimentoModel>> response) {
                createUpdateDB(response.body());
                Toast.makeText(context, "Atendimentos baixado", Toast.LENGTH_LONG).show();
            }
            @Override
            public void onFailure(Call<List<AtendimentoModel>> call, Throwable throwable) {
                Toast.makeText(context, "Falha ao baixar atendimentos", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void pacientes(String equipe, String token, final FragmentManager fragmentManager) {
        PacienteServices api = RetrofitClient.getRetrofitInstance().create(PacienteServices.class);
        Call<List<PacienteModel>> call = api.listPacientes(equipe, token);
        call.enqueue(new Callback<List<PacienteModel>>() {
            @Override
            public void onResponse(Call<List<PacienteModel>> call, Response<List<PacienteModel>> response) {
                // Salva o paciente no banco de dados
                createUpdateDB(response.body());
                // Recarrega o fragmento de pacientes
                Fragment pacienteFragment = PacienteFragment.newInstance();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.container_list_frag, pacienteFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                // Mensagem de baixado com sucesso
                Toast.makeText(context, "Pacientes baixado", Toast.LENGTH_LONG).show();
            }
            @Override
            public void onFailure(Call<List<PacienteModel>> call, Throwable throwable) {
                Toast.makeText(context, "Falha ao baixar pacientes", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void profissionais(String equipe, String token) {
        ProfissionalServices api = RetrofitClient.getRetrofitInstance().create(ProfissionalServices.class);
        Call<List<ProfissionalModel>> call = api.listProfissionais(equipe, token);
        call.enqueue(new Callback<List<ProfissionalModel>>() {
            @Override
            public void onResponse(Call<List<ProfissionalModel>> call, Response<List<ProfissionalModel>> response) {
                createUpdateDB(response.body());
                Toast.makeText(context, "Profissionais baixado", Toast.LENGTH_LONG).show();
            }
            @Override
            public void onFailure(Call<List<ProfissionalModel>> call, Throwable throwable) {
                Toast.makeText(context, "Falha ao baixar profissionais", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void localAtendimento() {
        LocalAtendimentoServices api = RetrofitClient.getRetrofitInstance().create(LocalAtendimentoServices.class);
        Call<List<LocalAtendimentoModel>> call = api.listLocalAtendimentos();
        call.enqueue(new Callback<List<LocalAtendimentoModel>>() {
            @Override
            public void onResponse(Call<List<LocalAtendimentoModel>> call, Response<List<LocalAtendimentoModel>> response) {
                createUpdateDB(response.body());
            }
            @Override
            public void onFailure(Call<List<LocalAtendimentoModel>> call, Throwable throwable) {
                //Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void modalidade() {
        ModalidadeServices api = RetrofitClient.getRetrofitInstance().create(ModalidadeServices.class);
        Call<List<ModalidadeModel>> call = api.listModalidades();
        call.enqueue(new Callback<List<ModalidadeModel>>() {
            @Override
            public void onResponse(Call<List<ModalidadeModel>> call, Response<List<ModalidadeModel>> response) {
                createUpdateDB(response.body());
            }
            @Override
            public void onFailure(Call<List<ModalidadeModel>> call, Throwable throwable) {
                //Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void tipoAtendimento() {
        TipoAtendimentoServices api = RetrofitClient.getRetrofitInstance().create(TipoAtendimentoServices.class);
        Call<List<TipoAtendimentoModel>> call = api.listTipoAtendimentos();
        call.enqueue(new Callback<List<TipoAtendimentoModel>>() {
            @Override
            public void onResponse(Call<List<TipoAtendimentoModel>> call, Response<List<TipoAtendimentoModel>> response) {
                createUpdateDB(response.body());
            }
            @Override
            public void onFailure(Call<List<TipoAtendimentoModel>> call, Throwable throwable) {
                //Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void condicaoAvaliada() {
        CondicaoAvaliadaServices api = RetrofitClient.getRetrofitInstance().create(CondicaoAvaliadaServices.class);
        Call<List<CondicaoAvaliadaModel>> call = api.listCondicoesAvaliadas();
        call.enqueue(new Callback<List<CondicaoAvaliadaModel>>() {
            @Override
            public void onResponse(Call<List<CondicaoAvaliadaModel>> call, Response<List<CondicaoAvaliadaModel>> response) {
                createUpdateDB(response.body());
            }
            @Override
            public void onFailure(Call<List<CondicaoAvaliadaModel>> call, Throwable throwable) {
                //Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void condutaDesfecho() {
        CondutaDesfechoServices api = RetrofitClient.getRetrofitInstance().create(CondutaDesfechoServices.class);
        Call<List<CondutaDesfechoModel>> call = api.listCondutaDesfechos();
        call.enqueue(new Callback<List<CondutaDesfechoModel>>() {
            @Override
            public void onResponse(Call<List<CondutaDesfechoModel>> call, Response<List<CondutaDesfechoModel>> response) {
                createUpdateDB(response.body());
            }
            @Override
            public void onFailure(Call<List<CondutaDesfechoModel>> call, Throwable throwable) {
                //Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void procedimento() {
        ProcedimentoServices api = RetrofitClient.getRetrofitInstance().create(ProcedimentoServices.class);
        Call<List<ProcedimentoModel>> call = api.listProcedimentos();
        call.enqueue(new Callback<List<ProcedimentoModel>>() {
            @Override
            public void onResponse(Call<List<ProcedimentoModel>> call, Response<List<ProcedimentoModel>> response) {
                createUpdateDB(response.body());
            }
            @Override
            public void onFailure(Call<List<ProcedimentoModel>> call, Throwable throwable) {
                //Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void turno() {
        TurnoServices api = RetrofitClient.getRetrofitInstance().create(TurnoServices.class);
        Call<List<TurnoModel>> call = api.listTurnos();
        call.enqueue(new Callback<List<TurnoModel>>() {
            @Override
            public void onResponse(Call<List<TurnoModel>> call, Response<List<TurnoModel>> response) {
                createUpdateDB(response.body());
            }
            @Override
            public void onFailure(Call<List<TurnoModel>> call, Throwable throwable) {
                //Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void createUpdateDB(List data) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(data);
        realm.commitTransaction();
        realm.close();
    }

    private List<AtendimentoModel> getAtendimentoDB() {
        List<AtendimentoModel> atendimentos;
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        atendimentos = realm.where(AtendimentoModel.class).equalTo("sync", false).findAll();
        realm.commitTransaction();
        realm.close();
        return atendimentos;
    }

    private void deleteAtendimentoDB(int ate_id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        AtendimentoModel atedimento = realm.where(AtendimentoModel.class).equalTo("ate_id", ate_id).findFirst();
        atedimento.deleteFromRealm();
        realm.commitTransaction();
        realm.close();
    }

}