package br.com.anderson.mas_sad.api.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TurnoModel extends RealmObject {
    @PrimaryKey
    @SerializedName("tur_id")
    @Expose
    private int tur_id;

    @SerializedName("tur_descricao")
    @Expose
    private String tur_descricao;

    @SerializedName("tur_codigo")
    @Expose
    private String tur_codigo;

    public TurnoModel() {

    }

    public TurnoModel(int tur_id, String tur_descricao, String tur_codigo) {
        this.tur_id = tur_id;
        this.tur_descricao = tur_descricao;
        this.tur_codigo = tur_codigo;
    }

    public int getTur_id() {
        return tur_id;
    }

    public void setTur_id(int tur_id) {
        this.tur_id = tur_id;
    }

    public String getTur_descricao() {
        return tur_descricao;
    }

    public void setTur_descricao(String tur_descricao) {
        this.tur_descricao = tur_descricao;
    }

    public String getTur_codigo() {
        return tur_codigo;
    }

    public void setTur_codigo(String tur_codigo) {
        this.tur_codigo = tur_codigo;
    }

    @NonNull
    @Override
    public String toString() {
        return tur_descricao;
    }
}
