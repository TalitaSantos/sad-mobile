package br.com.anderson.mas_sad.api.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class LocalAtendimentoModel extends RealmObject {
    @PrimaryKey
    @SerializedName("loc_id")
    @Expose
    private int loc_id;

    @SerializedName("loc_nome")
    @Expose
    private String loc_nome;

    @SerializedName("loc_codigo")
    @Expose
    private String loc_codigo;

    public LocalAtendimentoModel() {

    }

    public LocalAtendimentoModel(int loc_id, String loc_nome, String loc_codigo) {
        this.loc_id = loc_id;
        this.loc_nome = loc_nome;
        this.loc_codigo = loc_codigo;
    }

    public int getLoc_id() {
        return loc_id;
    }

    public void setLoc_id(int loc_id) {
        this.loc_id = loc_id;
    }

    public String getLoc_nome() {
        return loc_nome;
    }

    public void setLoc_nome(String loc_nome) {
        this.loc_nome = loc_nome;
    }

    public String getLoc_codigo() {
        return loc_codigo;
    }

    public void setLoc_codigo(String loc_codigo) {
        this.loc_codigo = loc_codigo;
    }

    @NonNull
    @Override
    public String toString() {
        return loc_nome;
    }
}
