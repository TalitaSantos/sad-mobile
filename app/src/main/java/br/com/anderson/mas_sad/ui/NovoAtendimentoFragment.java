package br.com.anderson.mas_sad.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.anderson.mas_sad.MainActivity;
import br.com.anderson.mas_sad.MultiSpinnerCondicao;
import br.com.anderson.mas_sad.MultiSpinnerProcedimento;
import br.com.anderson.mas_sad.R;
import br.com.anderson.mas_sad.api.model.AtendimentoModel;
import br.com.anderson.mas_sad.api.model.CondicaoAvaliadaModel;
import br.com.anderson.mas_sad.api.model.CondutaDesfechoModel;
import br.com.anderson.mas_sad.api.model.LocalAtendimentoModel;
import br.com.anderson.mas_sad.api.model.ModalidadeModel;
import br.com.anderson.mas_sad.api.model.PacienteModel;
import br.com.anderson.mas_sad.api.model.ProcedimentoModel;
import br.com.anderson.mas_sad.api.model.ProfissionalModel;
import br.com.anderson.mas_sad.api.model.TipoAtendimentoModel;
import br.com.anderson.mas_sad.api.model.TurnoModel;
import io.realm.Realm;
import io.realm.RealmResults;

public class NovoAtendimentoFragment extends Fragment {

    private Button btnSalvarId;
    private Button btnAddProcedimentos;
    private Spinner spinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_novo_atendimento, container, false);

        localSpinner(view);
        pacienteSpinner(view);
        profissionalSpinner(view);
        modalidadeSpinner(view);
        tipoSpinner(view);
        condicaoAvaliadaSpinner(view);
        procedimentoSpinner(view);
        condutaDesfechoSpinner(view);

        btnSalvarId = view.findViewById(R.id.btnSalvarId);
        btnSalvarId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enviar(view);
            }
        });


        final LinearLayout llEditTextsContainer = (LinearLayout) view.findViewById(R.id.ll_edit_texts_container);
        btnAddProcedimentos = view.findViewById(R.id.btnAddProcedimentos);
        btnAddProcedimentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                float scale = getResources().getDisplayMetrics().density;;

                EditText teste = new EditText(getActivity());
                teste.setBackgroundColor(Color.WHITE);
                teste.setBackgroundResource(R.drawable.edittext_bg);
                teste.setTextColor(Color.BLACK);
                teste.setPadding((int) (10*scale + 0.5f), 0, (int) (10*scale + 0.5f), 0);
                teste.setHeight((int) (44*scale + 0.5f));
                llEditTextsContainer.addView(teste);
            }
        });


        return view;

//        spinnerFn(pacientes, (Spinner) view.findViewById(R.id.spinnerPaciente));
//        spinnerFn(profissionais, (Spinner) view.findViewById(R.id.spinnerProfissional));
////        spinnerFn(locais, (Spinner) view.findViewById(R.id.spinnerLocal));
//        spinnerFn(modalidades, (Spinner) view.findViewById(R.id.spinnerModalidade));
//        spinnerFn(tipos_atendimento, (Spinner) view.findViewById(R.id.spinnerTipo));
//        spinnerFn(condicoes, (Spinner) view.findViewById(R.id.spinnerCondicao));
//        spinnerFn(procedimentos, (Spinner) view.findViewById(R.id.spinnerProcedimento));
//        spinnerFn(conduta, (Spinner) view.findViewById(R.id.spinnerConduta));


    }

//    public void spinnerFn(String[] itens, Spinner spinnerData) {
//        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(
//                getActivity(),
//                android.R.layout.simple_spinner_dropdown_item,
//                itens
//        );
//
//        spinnerData.setAdapter(adaptador);
//        spinnerData.setSelection(0);
//        spinnerData.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }

    public void enviar(View v) {
        Spinner paciente = getView().findViewById(R.id.spinnerPaciente);
        final PacienteModel pacienteObj = (PacienteModel) paciente.getSelectedItem();

        Spinner profissional = getView().findViewById(R.id.spinnerProfissional);
        final ProfissionalModel profissionalObj = (ProfissionalModel) profissional.getSelectedItem();

        Spinner localAtendimento = getView().findViewById(R.id.spinnerLocal);
        final LocalAtendimentoModel localAtendimentoObj = (LocalAtendimentoModel) localAtendimento.getSelectedItem();

        Spinner modalidade = getView().findViewById(R.id.spinnerModalidade);
        final ModalidadeModel modalidadeObj = (ModalidadeModel) modalidade.getSelectedItem();

        Spinner tipoAtendimento = getView().findViewById(R.id.spinnerTipo);
        final TipoAtendimentoModel tipoAtendimentoObj = (TipoAtendimentoModel) tipoAtendimento.getSelectedItem();

        Spinner condutaDesfecho = getView().findViewById(R.id.spinnerConduta);
        final CondutaDesfechoModel condutaDesfechoObj = (CondutaDesfechoModel) condutaDesfecho.getSelectedItem();

        MultiSpinnerCondicao condicaoAvaliada = getView().findViewById(R.id.spinnerCondicao);
        //condicaoAvaliada.getSelectedItems();
        MultiSpinnerProcedimento procedimento = getView().findViewById(R.id.spinnerProcedimento);
        //procedimento.getSelectedItems();

        final EditText cid10 = getView().findViewById(R.id.edittextCid10);
        final EditText ciap2 = getView().findViewById(R.id.edittextCiap2);
        final int turnoVal = turno(v);
        EditText procedimentoOutro = getView().findViewById(R.id.edittextProcedimentoOutro);


        if(pacienteObj.getPac_nome() != ""
                && profissionalObj.getPro_nome() != ""
                && localAtendimentoObj.getLoc_nome() != ""
                && modalidadeObj.getMod_descricao() != "" || tipoAtendimentoObj.getTat_id() == 3
                && tipoAtendimentoObj.getTat_nome() != ""
                && condutaDesfechoObj.getCde_descricao() != "") {


            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    // ID Autoincremento
                    Number ultimoId = realm.where(AtendimentoModel.class).max("ate_id");
                    int nextId = (ultimoId == null) ? 1 : ultimoId.intValue() + 100;
                    // Retorna o Turno
                    TurnoModel turno = realm.where(TurnoModel.class).equalTo("tur_id", turnoVal).findFirst();
                    // Data atual
                    Date date = new Date();

                    AtendimentoModel atendimento = new AtendimentoModel();
                    atendimento.setAte_id(nextId);
                    atendimento.setSync(false);
                    atendimento.setPac_id(pacienteObj.getPac_id());
                    atendimento.setPaciente(pacienteObj);
                    atendimento.setPro_id(profissionalObj.getPro_id());
                    atendimento.setProfissional(profissionalObj);
                    atendimento.setLoc_id(localAtendimentoObj.getLoc_id());
                    atendimento.setLocal_atendimento(localAtendimentoObj);
                    atendimento.setMod_id(modalidadeObj.getMod_id());
                    atendimento.setModalidade(modalidadeObj);
                    atendimento.setTat_id(tipoAtendimentoObj.getTat_id());
                    atendimento.setTipo_atendimento(tipoAtendimentoObj);
                    atendimento.setCde_id(condutaDesfechoObj.getCde_id());
                    atendimento.setConduta_desfecho(condutaDesfechoObj);
                    // Condição avaliada
                    // Procedimento
                    atendimento.setAte_cid10(cid10.getText().toString());
                    atendimento.setAte_ciap2(ciap2.getText().toString());

                    atendimento.setTur_id(turnoVal);
                    atendimento.setTurno(turno);
                    // Outro procedimento

                    atendimento.setAte_data(date);

//                    realm.insert(atendimento);
                    realm.copyToRealmOrUpdate(atendimento);
                }
            });


            Intent intent = new Intent(getActivity(), MainActivity.class);
            startActivity(intent);

            Toast.makeText(getContext(), "Atendimento salvo com sucesso", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "Preencha os campos obrigatórios!", Toast.LENGTH_LONG).show();
        }
    }

    public int turno(View view) {
        RadioGroup turnoId = view.findViewById(R.id.turnoId);
        int radioButtonId = turnoId.getCheckedRadioButtonId();
        int value = 1;

        switch (radioButtonId) {
            case R.id.turnoManhaId:
                value = 1;
                break;
            case R.id.turnoTardeId:
                value = 2;
                break;
            case R.id.turnoNoiteId:
                value = 3;
                break;
        }
        return value;
    }

//    private TurnoModel turnoObj(int tur_id) {
//        Realm realm = Realm.getDefaultInstance();
//        realm.beginTransaction();
//
////        TurnoModel turno = realm.where(TurnoModel.class).equalTo("tur_id", tur_id).findFirst();
//        List<TurnoModel> turno = realm.where(TurnoModel.class).findAll();
//
//        Log.e("Truno tesste", "***********" + turno);
//
//        realm.commitTransaction();
//        realm.close();
//        return turno.get(0);
//    }

    public static NovoAtendimentoFragment  newInstance() {
        return new NovoAtendimentoFragment ();
    }

















    // TODO
    private void pacienteSpinner(View view) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<PacienteModel> data = realm.where(PacienteModel.class).findAll();
        List<PacienteModel> pList = new ArrayList<>();
        // TODO
        pList.add(new PacienteModel(-1, "", "", "", "", "", "", "", "", null, null, -1, -1));

        for (int i = 0; i < data.size(); i++) {
            PacienteModel t = new PacienteModel(
                    data.get(i).getPac_id(),
                    data.get(i).getPac_nome(),
                    data.get(i).getPac_cpf(),
                    data.get(i).getPac_endereco(),
                    data.get(i).getPac_cep(),
                    data.get(i).getPac_email(),
                    data.get(i).getPac_telefone(),
                    data.get(i).getPac_cns(),
                    data.get(i).getPac_data_nascimento(),
                    data.get(i).getSexo(),
                    data.get(i).getEquipe(),
                    data.get(i).getSex_id(),
                    data.get(i).getEqu_id()
            );
            pList.add(t);
        }

        realm.commitTransaction();
        realm.close();

        spinner = view.findViewById(R.id.spinnerPaciente);
        ArrayAdapter<PacienteModel> adapter = new ArrayAdapter<>(
                getContext(), android.R.layout.simple_spinner_item, pList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // TODO
    private void profissionalSpinner(View view) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<ProfissionalModel> data = realm.where(ProfissionalModel.class).findAll();
        List<ProfissionalModel> pList = new ArrayList<>();
        // TODO
        pList.add(new ProfissionalModel(-1, "", "", "", "", "", "", "", "", "", null, -1, null, -1));

        for (int i = 0; i < data.size(); i++) {
            ProfissionalModel t = new ProfissionalModel(
                    data.get(i).getPro_id(),
                    data.get(i).getPro_nome(),
                    data.get(i).getPro_cpf(),
                    data.get(i).getPro_endereco(),
                    data.get(i).getPro_cep(),
                    data.get(i).getPro_email(),
                    data.get(i).getPro_telefone(),
                    data.get(i).getPro_cns(),
                    data.get(i).getPro_cbo(),
                    data.get(i).getPro_data_nascimento(),
                    data.get(i).getSexo(),
                    data.get(i).getSex_id(),
                    data.get(i).getEquipe(),
                    data.get(i).getEqu_id()
            );
            pList.add(t);
        }

        realm.commitTransaction();
        realm.close();

        spinner = view.findViewById(R.id.spinnerProfissional);
        ArrayAdapter<ProfissionalModel> adapter = new ArrayAdapter<>(
                getContext(), android.R.layout.simple_spinner_item, pList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // TODO
    private void localSpinner(View view) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<LocalAtendimentoModel> data = realm.where(LocalAtendimentoModel.class).findAll();
        List<LocalAtendimentoModel> pList = new ArrayList<>();
        // TODO
        pList.add(new LocalAtendimentoModel(-1, "", ""));

        for (int i = 0; i < data.size(); i++) {
            LocalAtendimentoModel t = new LocalAtendimentoModel(
                    data.get(i).getLoc_id(),
                    data.get(i).getLoc_nome(),
                    data.get(i).getLoc_codigo()
            );
            pList.add(t);
        }

        realm.commitTransaction();
        realm.close();

        spinner = view.findViewById(R.id.spinnerLocal);
        ArrayAdapter<LocalAtendimentoModel> adapter = new ArrayAdapter<>(
                getContext(), android.R.layout.simple_spinner_item, pList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // TODO
    private void modalidadeSpinner(View view) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<ModalidadeModel> data = realm.where(ModalidadeModel.class).findAll();
        List<ModalidadeModel> pList = new ArrayList<>();
        // TODO
        pList.add(new ModalidadeModel(-1, "", ""));

        for (int i = 0; i < data.size(); i++) {
            ModalidadeModel t = new ModalidadeModel(
                    data.get(i).getMod_id(),
                    data.get(i).getMod_descricao(),
                    data.get(i).getMod_codigo()
            );
            pList.add(t);
        }

        realm.commitTransaction();
        realm.close();

        spinner = view.findViewById(R.id.spinnerModalidade);
        ArrayAdapter<ModalidadeModel> adapter = new ArrayAdapter<>(
                getContext(), android.R.layout.simple_spinner_item, pList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // TODO
    private void tipoSpinner(View view) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<TipoAtendimentoModel> data = realm.where(TipoAtendimentoModel.class).findAll();
        List<TipoAtendimentoModel> pList = new ArrayList<>();
        // TODO
        pList.add(new TipoAtendimentoModel(-1, "", ""));

        for (int i = 0; i < data.size(); i++) {
            TipoAtendimentoModel t = new TipoAtendimentoModel(
                    data.get(i).getTat_id(),
                    data.get(i).getTat_nome(),
                    data.get(i).getTat_codigo()
            );
            pList.add(t);
        }

        realm.commitTransaction();
        realm.close();

        spinner = view.findViewById(R.id.spinnerTipo);
        ArrayAdapter<TipoAtendimentoModel> adapter = new ArrayAdapter<>(
                getContext(), android.R.layout.simple_spinner_item, pList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

                // TODO
                Spinner getModalidadeSpinner = getView().findViewById(R.id.spinnerModalidade);
                TextView getModalidadeSpinnerLabel = getView().findViewById(R.id.spinnerModalidadeLabel);
                Spinner getCondicaoSpinner = getView().findViewById(R.id.spinnerCondicao);
                TextView getCondicaoSpinnerLabel = getView().findViewById(R.id.spinnerCondicaoLabel);
                Spinner getProcedimentoSpinner = getView().findViewById(R.id.spinnerProcedimento);
                TextView getProcedimentoSpinnerLabel = getView().findViewById(R.id.spinnerProcedimentoLabel);
                EditText getProcedimentoOutroSpinner = getView().findViewById(R.id.edittextProcedimentoOutro);
                TextView getProcedimentoOutroSpinnerLabel = getView().findViewById(R.id.spinnerProcedimentoOutroLabel);

                if(id == 3) {
                    getModalidadeSpinner.setEnabled(false);
                    getModalidadeSpinner.setAlpha(0.5f);
                    getModalidadeSpinnerLabel.setEnabled(false);
                    getModalidadeSpinnerLabel.setAlpha(0.5f);

                    getCondicaoSpinner.setEnabled(false);
                    getCondicaoSpinner.setAlpha(0.5f);
                    getCondicaoSpinnerLabel.setEnabled(false);
                    getCondicaoSpinnerLabel.setAlpha(0.5f);

                    getProcedimentoSpinner.setEnabled(false);
                    getProcedimentoSpinner.setAlpha(0.5f);
                    getProcedimentoSpinnerLabel.setEnabled(false);
                    getProcedimentoSpinnerLabel.setAlpha(0.5f);

                    getProcedimentoOutroSpinner.setEnabled(false);
                    getProcedimentoOutroSpinner.setAlpha(0.5f);
                    getProcedimentoOutroSpinnerLabel.setEnabled(false);
                    getProcedimentoOutroSpinnerLabel.setAlpha(0.5f);
                } else {
                    getModalidadeSpinner.setEnabled(true);
                    getModalidadeSpinner.setAlpha(1);
                    getModalidadeSpinnerLabel.setEnabled(true);
                    getModalidadeSpinnerLabel.setAlpha(1);

                    getCondicaoSpinner.setEnabled(true);
                    getCondicaoSpinner.setAlpha(1);
                    getCondicaoSpinnerLabel.setEnabled(true);
                    getCondicaoSpinnerLabel.setAlpha(1);

                    getProcedimentoSpinner.setEnabled(true);
                    getProcedimentoSpinner.setAlpha(1);
                    getProcedimentoSpinnerLabel.setEnabled(true);
                    getProcedimentoSpinnerLabel.setAlpha(1);

                    getProcedimentoOutroSpinner.setEnabled(true);
                    getProcedimentoOutroSpinner.setAlpha(1);
                    getProcedimentoOutroSpinnerLabel.setEnabled(true);
                    getProcedimentoOutroSpinnerLabel.setAlpha(1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // TODO
    private void condicaoAvaliadaSpinner(View view) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<CondicaoAvaliadaModel> data = realm.where(CondicaoAvaliadaModel.class).findAll();
        List<CondicaoAvaliadaModel> pList = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            CondicaoAvaliadaModel t = new CondicaoAvaliadaModel(
                    data.get(i).getCon_id(),
                    data.get(i).getCon_nome(),
                    data.get(i).getCon_codigo()
            );
            pList.add(t);
        }

        realm.commitTransaction();
        realm.close();

        MultiSpinnerCondicao mySpinner;
        mySpinner = view.findViewById(R.id.spinnerCondicao);
        mySpinner.setItems(pList);

        // To get the selected Item list
        List<CondicaoAvaliadaModel> selectedItems = mySpinner.getSelectedItems();

    }

    // TODO
    private void procedimentoSpinner(View view) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<ProcedimentoModel> data = realm.where(ProcedimentoModel.class).findAll();
        List<ProcedimentoModel> pList = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {
            ProcedimentoModel t = new ProcedimentoModel(
                    data.get(i).getPce_id(),
                    data.get(i).getPce_nome(),
                    data.get(i).getPce_codigo()
            );
            pList.add(t);
        }

        realm.commitTransaction();
        realm.close();

        MultiSpinnerProcedimento mySpinner;
        mySpinner = view.findViewById(R.id.spinnerProcedimento);
        mySpinner.setItems(pList);

        // To get the selected Item list
        List<ProcedimentoModel> selectedItems = mySpinner.getSelectedItems();
    }

    // TODO
    private void condutaDesfechoSpinner(View view) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();

        RealmResults<CondutaDesfechoModel> data = realm.where(CondutaDesfechoModel.class).findAll();
        List<CondutaDesfechoModel> pList = new ArrayList<>();
        // TODO
        pList.add(new CondutaDesfechoModel(-1, "", ""));

        for (int i = 0; i < data.size(); i++) {
            CondutaDesfechoModel t = new CondutaDesfechoModel(
                    data.get(i).getCde_id(),
                    data.get(i).getCde_descricao(),
                    data.get(i).getCde_codigo()
            );
            pList.add(t);
        }

        realm.commitTransaction();
        realm.close();

        spinner = view.findViewById(R.id.spinnerConduta);
        ArrayAdapter<CondutaDesfechoModel> adapter = new ArrayAdapter<>(
                getContext(), android.R.layout.simple_spinner_item, pList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

}