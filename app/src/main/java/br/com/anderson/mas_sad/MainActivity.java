package br.com.anderson.mas_sad;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import br.com.anderson.mas_sad.api.model.AtendimentoModel;
import br.com.anderson.mas_sad.api.model.PacienteModel;
import br.com.anderson.mas_sad.api.model.ProfissionalModel;
import br.com.anderson.mas_sad.ui.PacienteFragment;
import br.com.anderson.mas_sad.ui.ConfiguracaoFragment;
import br.com.anderson.mas_sad.utils.NavMenu;
import io.realm.Realm;

public class MainActivity extends NavMenu{

    private PreferenceHelper preferenceHelper;
    private Sincronizar sincronizar = new Sincronizar(MainActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Verifica se está logado: se não está logado irá para a tela de Login
        preferenceHelper = new PreferenceHelper(this);
        if(!preferenceHelper.getIsLogin()) {
            Intent intent = new Intent(MainActivity.this,Login.class);
            startActivity(intent);
            this.finish();
        } else {
            // Criando toolbar
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setTitle("");
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_button);

            // Recupera o id da equipe armazenado no sharedpreference
            String equipe = Integer.toString(preferenceHelper.getEquipe());
            String token = preferenceHelper.getToken();
            // Armazena no banco de dados os pacientes, profissionais e atendimentos

            sincronizar.pacientes(equipe, token, getSupportFragmentManager());
            sincronizar.profissionais(equipe, token);
            sincronizar.atendimentos(equipe, token);

            openFragment();
            //openFragmentList();
        }

        this.startMenu(savedInstanceState, this);
    }

    private void openFragment() {
        Fragment confFragment = ConfiguracaoFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, confFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void openFragmentList() {
        Fragment listFragment = PacienteFragment.newInstance();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container_list_frag, listFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onResume(){
        super.onResume();

        mDrawer.resetDrawerContent();
        mDrawer.setSelection(MENU_PACIENTES);
    }

    @Override
    public void onBackPressed() {
        if (super.mDrawer.isDrawerOpen()) {
            super.mDrawer.closeDrawer();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                if(super.mDrawer.isDrawerOpen()){
                    super.mDrawer.closeDrawer();
                }else {
                    super.mDrawer.openDrawer();
                }
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void menuItemSelected(int activity) {
        switch(activity) {
            case (NavMenu.MENU_DASHBOARD):
                mDrawer.closeDrawer();
                break;
            case NavMenu.MENU_PACIENTES:
                mDrawer.closeDrawer();
                break;
            case NavMenu.MENU_SINCRONIZACAO:
                confirmSync();
                break;
            case NavMenu.MENU_SAIR:
                confirmExit();
                break;
            default:
                break;
        }
    }

    public void confirmExit(){
        new LovelyStandardDialog(this)
                .setTopColorRes(R.color.colorPrimary)
                .setTopTitle("Sair")
                .setTopTitleColor(this.getResources().getColor(R.color.md_white_1000))
                .setIcon(R.drawable.ic_exit_to_app_white)
                .setMessage("Deseja Realmente Sair?")
                .setMessageGravity(Gravity.CENTER)
                .setButtonsColor(getResources().getColor(R.color.md_white_1000))
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deslogarUsuario();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    public void confirmSync(){
        new LovelyStandardDialog(this)
                .setTopColorRes(R.color.colorPrimary)
                .setTopTitle("Sincronização")
                .setTopTitleColor(this.getResources().getColor(R.color.md_white_1000))
                .setIcon(R.drawable.ic_sync_black_white)
                .setMessage("Deseja sincronizar?")
                .setMessageGravity(Gravity.CENTER)
                .setButtonsColor(getResources().getColor(R.color.md_white_1000))
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sync();
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .show();
    }

    private void deslogarUsuario() {
        preferenceHelper.putIsLogin(false);
        preferenceHelper.putId(-1);
        preferenceHelper.putNome("");
        preferenceHelper.putCns("");
        preferenceHelper.putCbo("");
        preferenceHelper.putEmail("");
        preferenceHelper.putEquipe(-1);
        preferenceHelper.putEquipeNome("");
        preferenceHelper.putEquipeIne("");
        preferenceHelper.putUnidadeNome("");
        preferenceHelper.putUnidadeCnes("");
        preferenceHelper.putToken("");

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.delete(AtendimentoModel.class);
        realm.delete(PacienteModel.class);
        realm.delete(ProfissionalModel.class);
        realm.commitTransaction();
        realm.close();

        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
        this.finish();
    }

    private void sync(){
        Toast.makeText(this, "Sincronizando...!", Toast.LENGTH_SHORT).show();
        // Recupera o id da equipe armazenado no sharedpreference
        String equipe = Integer.toString(preferenceHelper.getEquipe());
        String token = preferenceHelper.getToken();
        sincronizar.atendimentoSalvar(equipe, token);
        // Armazena no banco de dados os pacientes, profissionais e atendimentos
        sincronizar.atendimentos(equipe, token);
        sincronizar.profissionais(equipe, token);
        sincronizar.pacientes(equipe, token, getSupportFragmentManager());
        // Armazena no banco de dados as tabelas de domínio
        sincronizar.localAtendimento();
        sincronizar.modalidade();
        sincronizar.tipoAtendimento();
        sincronizar.condicaoAvaliada();
        sincronizar.condutaDesfecho();
        sincronizar.procedimento();
        sincronizar.turno();
    }
}
