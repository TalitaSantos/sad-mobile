package br.com.anderson.mas_sad.api.service;

import java.util.List;

import br.com.anderson.mas_sad.api.model.AtendimentoModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AtendimentoServices {
    @GET("atendimento/{equipe}")
    Call<List<AtendimentoModel>> listAtendimentos(
            @Path("equipe") String atendimento,
            @Header("x-access-token") String token
    );

    @POST("atendimento/{equipe}")
    Call<AtendimentoModel> atendimento(
            @Path("equipe") String atendimento,
            @Header("x-access-token") String token,
            @Body AtendimentoModel data);
}
