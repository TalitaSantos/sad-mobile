package br.com.anderson.mas_sad.api.service;

import java.util.List;

import br.com.anderson.mas_sad.api.model.PacienteModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface PacienteServices {
    @GET("paciente/{equipe}")
    Call<List<PacienteModel>> listPacientes(
            @Path("equipe") String paciente,
            @Header("x-access-token") String token
    );
}
