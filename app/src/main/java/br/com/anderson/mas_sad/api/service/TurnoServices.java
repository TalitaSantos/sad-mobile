package br.com.anderson.mas_sad.api.service;

import java.util.List;
import br.com.anderson.mas_sad.api.model.TurnoModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface TurnoServices {
    @GET("turno")
    Call<List<TurnoModel>> listTurnos();
}
