package br.com.anderson.mas_sad.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.Date;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AtendimentoModel extends RealmObject {
    @PrimaryKey
    @SerializedName("ate_id")
    @Expose
    private int ate_id;

    @SerializedName("ate_nome")
    @Expose
    private String ate_nome;

    @SerializedName("turno")
    @Expose
    private TurnoModel turno;

    @SerializedName("tur_id")
    @Expose
    private int tur_id;

    @SerializedName("paciente")
    @Expose
    private PacienteModel paciente;

    @SerializedName("pac_id")
    @Expose
    private int pac_id;

    @SerializedName("localAtendimento")
    @Expose
    private LocalAtendimentoModel local_atendimento;

    @SerializedName("loc_id")
    @Expose
    private int loc_id;

    @SerializedName("modalidade")
    @Expose
    private ModalidadeModel modalidade;

    @SerializedName("mod_id")
    @Expose
    private int mod_id;

    @SerializedName("tipoAtendimento")
    @Expose
    private TipoAtendimentoModel tipo_atendimento;

    @SerializedName("tat_id")
    @Expose
    private int tat_id;

    @SerializedName("condutaDesfecho")
    @Expose
    private CondutaDesfechoModel conduta_desfecho;

    @SerializedName("cde_id")
    @Expose
    private int cde_id;

    @SerializedName("ate_cid10")
    @Expose
    private String ate_cid10;

    @SerializedName("ate_ciap2")
    @Expose
    private String ate_ciap2;

    @SerializedName("profissional")
    @Expose
    private ProfissionalModel profissional;

    @SerializedName("pro_id")
    @Expose
    private int pro_id;

    @SerializedName("ate_data")
    @Expose
    private Date ate_data;

    private Boolean sync = true;

    public AtendimentoModel() {

    }

    public AtendimentoModel(int ate_id, String ate_nome, TurnoModel turno, int tur_id,
                        PacienteModel paciente, int pac_id, LocalAtendimentoModel local_atendimento,
                        int loc_id, ModalidadeModel modalidade, int mod_id, TipoAtendimentoModel
                        tipo_atendimento, int tat_id, CondutaDesfechoModel conduta_desfecho,
                        int cde_id, String ate_cid10, String ate_ciap2,
                        ProfissionalModel profissional, int pro_id, Date ate_data, Boolean sync) {
        this.ate_id = ate_id;
        this.ate_nome = ate_nome;
        this.turno = turno;
        this.tur_id = tur_id;
        this.paciente = paciente;
        this.pac_id = pac_id;
        this.local_atendimento = local_atendimento;
        this.loc_id = loc_id;
        this.modalidade = modalidade;
        this.mod_id = mod_id;
        this.tipo_atendimento = tipo_atendimento;
        this.tat_id = tat_id;
        this.conduta_desfecho = conduta_desfecho;
        this.cde_id = cde_id;
        this.ate_cid10 = ate_cid10;
        this.ate_ciap2 = ate_ciap2;
        this.profissional = profissional;
        this.pro_id = pro_id;
        this.ate_data = ate_data;
        this.sync = sync;
    }

    public int getAte_id() {
        return ate_id;
    }

    public void setAte_id(int ate_id) {
        this.ate_id = ate_id;
    }

    public String getAte_nome() {
        return ate_nome;
    }

    public void setAte_nome(String ate_nome) {
        this.ate_nome = ate_nome;
    }

    public TurnoModel getTurno() {
        return turno;
    }

    public void setTurno(TurnoModel turno) {
        this.turno = turno;
    }

    public int getTur_id() {
        return tur_id;
    }

    public void setTur_id(int tur_id) {
        this.tur_id = tur_id;
    }

    public PacienteModel getPaciente() {
        return paciente;
    }

    public void setPaciente(PacienteModel paciente) {
        this.paciente = paciente;
    }

    public int getPac_id() {
        return pac_id;
    }

    public void setPac_id(int pac_id) {
        this.pac_id = pac_id;
    }

    public LocalAtendimentoModel getLocal_atendimento() {
        return local_atendimento;
    }

    public void setLocal_atendimento(LocalAtendimentoModel local_atendimento) {
        this.local_atendimento = local_atendimento;
    }

    public int getLoc_id() {
        return loc_id;
    }

    public void setLoc_id(int loc_id) {
        this.loc_id = loc_id;
    }

    public ModalidadeModel getModalidade() {
        return modalidade;
    }

    public void setModalidade(ModalidadeModel modalidade) {
        this.modalidade = modalidade;
    }

    public int getMod_id() {
        return mod_id;
    }

    public void setMod_id(int mod_id) {
        this.mod_id = mod_id;
    }

    public TipoAtendimentoModel getTipo_atendimento() {
        return tipo_atendimento;
    }

    public void setTipo_atendimento(TipoAtendimentoModel tipo_atendimento) {
        this.tipo_atendimento = tipo_atendimento;
    }

    public int getTat_id() {
        return tat_id;
    }

    public void setTat_id(int tat_id) {
        this.tat_id = tat_id;
    }

    public CondutaDesfechoModel getConduta_desfecho() {
        return conduta_desfecho;
    }

    public void setConduta_desfecho(CondutaDesfechoModel conduta_desfecho) {
        this.conduta_desfecho = conduta_desfecho;
    }

    public int getCde_id() {
        return cde_id;
    }

    public void setCde_id(int cde_id) {
        this.cde_id = cde_id;
    }

    public String getAte_cid10() {
        return ate_cid10;
    }

    public void setAte_cid10(String ate_cid10) {
        this.ate_cid10 = ate_cid10;
    }

    public String getAte_ciap2() {
        return ate_ciap2;
    }

    public void setAte_ciap2(String ate_ciap2) {
        this.ate_ciap2 = ate_ciap2;
    }

    public ProfissionalModel getProfissional() {
        return profissional;
    }

    public void setProfissional(ProfissionalModel profissional) {
        this.profissional = profissional;
    }

    public int getPro_id() {
        return pro_id;
    }

    public void setPro_id(int pro_id) {
        this.pro_id = pro_id;
    }

    public Date getAte_data() {
        return ate_data;
    }

    public void setAte_data(Date ate_data) {
        this.ate_data = ate_data;
    }

    public Boolean getSync() {
        return sync;
    }

    public void setSync(Boolean sync) {
        this.sync = sync;
    }
}
