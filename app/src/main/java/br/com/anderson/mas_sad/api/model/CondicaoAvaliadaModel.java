package br.com.anderson.mas_sad.api.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CondicaoAvaliadaModel extends RealmObject {
    @PrimaryKey
    @SerializedName("con_id")
    @Expose
    private int con_id;

    @SerializedName("con_nome")
    @Expose
    private String con_nome;

    @SerializedName("con_codigo")
    @Expose
    private String con_codigo;

    public CondicaoAvaliadaModel() {

    }

    public CondicaoAvaliadaModel(int con_id, String con_nome, String con_codigo) {
        this.con_id = con_id;
        this.con_nome = con_nome;
        this.con_codigo = con_codigo;
    }

    public int getCon_id() {
        return con_id;
    }

    public void setCon_id(int con_id) {
        this.con_id = con_id;
    }

    public String getCon_nome() {
        return con_nome;
    }

    public void setCon_nome(String con_nome) {
        this.con_nome = con_nome;
    }

    public String getCon_codigo() {
        return con_codigo;
    }

    public void setCon_codigo(String con_codigo) {
        this.con_codigo = con_codigo;
    }

    @NonNull
    @Override
    public String toString() {
        return con_nome;
    }
}
