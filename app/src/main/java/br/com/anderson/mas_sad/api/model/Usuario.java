package br.com.anderson.mas_sad.api.model;

import com.google.gson.annotations.SerializedName;

public class Usuario {
    @SerializedName("id")
    private int id;

    @SerializedName("nome")
    private String nome;

    @SerializedName("cns")
    private String cns;

    @SerializedName("cbo")
    private String cbo;

    @SerializedName("email")
    private String email;

    @SerializedName("equipe")
    private int equipe;

    @SerializedName("equipeNome")
    private String equipeNome;

    @SerializedName("equipeIne")
    private String equipeIne;

    @SerializedName("unidadeNome")
    private String unidadeNome;

    @SerializedName("unidadeCnes")
    private String unidadeCnes;

    @SerializedName("token")
    private String token;

    @SerializedName("status")
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCns() {
        return cns;
    }

    public void setCns(String cns) {
        this.cns = cns;
    }

    public String getCbo() {
        return cbo;
    }

    public void setCbo(String cbo) {
        this.cbo = cbo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getEquipe() {
        return equipe;
    }

    public void setEquipe(int equipe) {
        this.equipe = equipe;
    }

    public String getEquipeNome() {
        return equipeNome;
    }

    public void setEquipeNome(String equipeNome) {
        this.equipeNome = equipeNome;
    }

    public String getEquipeIne() {
        return equipeIne;
    }

    public void setEquipeIne(String equipeIne) {
        this.equipeIne = equipeIne;
    }

    public String getUnidadeNome() {
        return unidadeNome;
    }

    public void setUnidadeNome(String unidadeNome) {
        this.unidadeNome = unidadeNome;
    }

    public String getUnidadeCnes() {
        return unidadeCnes;
    }

    public void setUnidadeCnes(String unidadeCnes) {
        this.unidadeCnes = unidadeCnes;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
