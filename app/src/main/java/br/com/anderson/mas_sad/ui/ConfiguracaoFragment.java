package br.com.anderson.mas_sad.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import br.com.anderson.mas_sad.Login;
import br.com.anderson.mas_sad.PreferenceHelper;
import br.com.anderson.mas_sad.R;
import br.com.anderson.mas_sad.api.model.AtendimentoModel;
import br.com.anderson.mas_sad.api.model.PacienteModel;
import br.com.anderson.mas_sad.api.model.ProfissionalModel;
import io.realm.Realm;

public class ConfiguracaoFragment extends Fragment {

    private PreferenceHelper preferenceHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_configuracao, container, false);

        preferenceHelper = new PreferenceHelper(this.getActivity());

        TextView configNomeId = view.findViewById(R.id.configNomeId);
        TextView configCnsId = view.findViewById(R.id.configCnsId);
        TextView configCboId = view.findViewById(R.id.configCboId);
        TextView configUnidadeId = view.findViewById(R.id.configUnidadeId);
        TextView configCnesId = view.findViewById(R.id.configCnesId);
        TextView configEquipeId = view.findViewById(R.id.configEquipeId);
        TextView configIneId = view.findViewById(R.id.configIneId);

        configNomeId.setText(preferenceHelper.getNome() + "  -  " + preferenceHelper.getEmail());
        configCnsId.setText(preferenceHelper.getUnidadeCnes());
        configCboId.setText(preferenceHelper.getCbo());
        configUnidadeId.setText(preferenceHelper.getUnidadeNome());
        configCnesId.setText(preferenceHelper.getCns());
        configEquipeId.setText(preferenceHelper.getEquipeNome());
        configIneId.setText(preferenceHelper.getEquipeIne());

        return view;
    }
    public static ConfiguracaoFragment newInstance() {
        return new ConfiguracaoFragment();
    }

}