package br.com.anderson.mas_sad;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {
    private final String INTRO = "intro";
    private final String ID = "id";
    private final String NOME = "nome";
    private final String CNS = "cns";
    private final String CBO = "cbo";
    private final String EMAIL = "email";
    private final String EQUIPE = "equipe";
    private final String EQUIPENOME = "equipeNome";
    private final String EQUIPEINE = "equipeIne";
    private final String UNIDADENOME = "unidadeNome";
    private final String UNIDADECNES = "unidadeCnes";
    private final String TOKEN = "token";
    private SharedPreferences app_prefs;
    private Context context;

    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences("shared", Context.MODE_PRIVATE);
        this.context = context;
    }

    public void putIsLogin(boolean loginorout) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putBoolean(INTRO, loginorout);
        edit.commit();
    }
    public boolean getIsLogin() {
        return app_prefs.getBoolean(INTRO, false);
    }

    public void putId(int data) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putInt(ID, data);
        edit.commit();
    }
    public int getId() {
        return app_prefs.getInt(ID, -1);
    }

    public void putNome(String data) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(NOME, data);
        edit.commit();
    }
    public String getNome() {
        return app_prefs.getString(NOME, "");
    }

    public void putCns(String data) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(CNS, data);
        edit.commit();
    }
    public String getCns() {
        return app_prefs.getString(CNS, "");
    }

    public void putCbo(String data) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(CBO, data);
        edit.commit();
    }
    public String getCbo() {
        return app_prefs.getString(CBO, "");
    }

    public void putEmail(String data) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(EMAIL, data);
        edit.commit();
    }
    public String getEmail() {
        return app_prefs.getString(EMAIL, "");
    }

    public void putEquipe(int data) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putInt(EQUIPE, data);
        edit.commit();
    }
    public int getEquipe() {
        return app_prefs.getInt(EQUIPE, -1);
    }

    public void putEquipeNome(String data) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(EQUIPENOME, data);
        edit.commit();
    }
    public String getEquipeNome() {
        return app_prefs.getString(EQUIPENOME, "");
    }

    public void putEquipeIne(String data) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(EQUIPEINE, data);
        edit.commit();
    }
    public String getEquipeIne() {
        return app_prefs.getString(EQUIPEINE, "");
    }

    public void putUnidadeNome(String data) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(UNIDADENOME, data);
        edit.commit();
    }
    public String getUnidadeNome() {
        return app_prefs.getString(UNIDADENOME, "");
    }

    public void putUnidadeCnes(String data) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(UNIDADECNES, data);
        edit.commit();
    }
    public String getUnidadeCnes() {
        return app_prefs.getString(UNIDADECNES, "");
    }

    public void putToken(String data) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(TOKEN, data);
        edit.commit();
    }
    public String getToken() {
        return app_prefs.getString(TOKEN, "");
    }


}
