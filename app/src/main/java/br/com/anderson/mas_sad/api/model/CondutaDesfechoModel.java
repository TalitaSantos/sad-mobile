package br.com.anderson.mas_sad.api.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CondutaDesfechoModel extends RealmObject {
    @PrimaryKey
    @SerializedName("cde_id")
    @Expose
    private int cde_id;

    @SerializedName("cde_descricao")
    @Expose
    private String cde_descricao;

    @SerializedName("cde_codigo")
    @Expose
    private String cde_codigo;

    public CondutaDesfechoModel() {

    }

    public CondutaDesfechoModel(int cde_id, String cde_descricao, String cde_codigo) {
        this.cde_id = cde_id;
        this.cde_descricao = cde_descricao;
        this.cde_codigo = cde_codigo;
    }

    public int getCde_id() {
        return cde_id;
    }

    public void setCde_id(int cde_id) {
        this.cde_id = cde_id;
    }

    public String getCde_descricao() {
        return cde_descricao;
    }

    public void setCde_descricao(String cde_descricao) {
        this.cde_descricao = cde_descricao;
    }

    public String getCde_codigo() {
        return cde_codigo;
    }

    public void setCde_codigo(String cde_codigo) {
        this.cde_codigo = cde_codigo;
    }

    @NonNull
    @Override
    public String toString() {
        return cde_descricao;
    }
}
