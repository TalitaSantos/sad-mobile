package br.com.anderson.mas_sad.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class EquipeModel extends RealmObject {
    @PrimaryKey
    @SerializedName("equ_id")
    @Expose
    private int equ_id;

    @SerializedName("equ_nome")
    @Expose
    private String equ_nome;

    @SerializedName("uni_id")
    @Expose
    private int uni_id;

    @SerializedName("equ_ine")
    @Expose
    private String equ_ine;

    public EquipeModel() {

    }

    public EquipeModel(int equ_id, String equ_nome, int uni_id, String equ_ine) {
        this.equ_id = equ_id;
        this.equ_nome = equ_nome;
        this.uni_id = uni_id;
        this.equ_ine = equ_ine;
    }

    public int getEqu_id() {
        return equ_id;
    }

    public void setEqu_id(int equ_id) {
        this.equ_id = equ_id;
    }

    public String getEqu_nome() {
        return equ_nome;
    }

    public void setEqu_nome(String equ_nome) {
        this.equ_nome = equ_nome;
    }

    public int getUni_id() {
        return uni_id;
    }

    public void setUni_id(int uni_id) {
        this.uni_id = uni_id;
    }

    public String getEqu_ine() {
        return equ_ine;
    }

    public void setEqu_ine(String equ_ine) {
        this.equ_ine = equ_ine;
    }
}
