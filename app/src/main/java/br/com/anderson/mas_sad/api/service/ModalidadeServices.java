package br.com.anderson.mas_sad.api.service;

import java.util.List;
import br.com.anderson.mas_sad.api.model.ModalidadeModel;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ModalidadeServices {
    @GET("modalidade")
    Call<List<ModalidadeModel>> listModalidades();
}
