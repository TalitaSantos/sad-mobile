package br.com.anderson.mas_sad;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import br.com.anderson.mas_sad.api.model.LoginModel;
import br.com.anderson.mas_sad.api.model.Usuario;
import br.com.anderson.mas_sad.api.service.LoginServices;

public class Login extends AppCompatActivity {

    private Button btnSave;
    private EditText usuario;
    private EditText senha;
    private int tipo = 2;
    private PreferenceHelper preferenceHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        preferenceHelper = new PreferenceHelper(this);

        usuario = findViewById(R.id.usuarioId);
        senha = findViewById(R.id.senhaId);

        btnSave = findViewById(R.id.btnSaveId);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });

        Sincronizar sincronizar = new Sincronizar(Login.this);
        sincronizar.localAtendimento();
        sincronizar.modalidade();
        sincronizar.tipoAtendimento();
        sincronizar.condicaoAvaliada();
        sincronizar.condutaDesfecho();
        sincronizar.procedimento();
        sincronizar.turno();
    }

    private void loginUser() {
        final String formUsuario = usuario.getText().toString().trim();
        final String formSenha = senha.getText().toString().trim();

        LoginServices api = RetrofitClient.getRetrofitInstance().create(LoginServices.class);
        LoginModel loginModel = new LoginModel(formUsuario, formSenha, tipo);
        Call<Usuario> call = api.login(loginModel);

        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {

                        Usuario userResponse = response.body();
                        preferenceHelper.putIsLogin(true);
                        preferenceHelper.putId(userResponse.getId());
                        preferenceHelper.putNome(userResponse.getNome());
                        preferenceHelper.putCns(userResponse.getCns());
                        preferenceHelper.putCbo(userResponse.getCbo());
                        preferenceHelper.putEmail(userResponse.getEmail());
                        preferenceHelper.putEquipe(userResponse.getEquipe());
                        preferenceHelper.putEquipeNome(userResponse.getEquipeNome());
                        preferenceHelper.putEquipeIne(userResponse.getEquipeIne());
                        preferenceHelper.putUnidadeNome(userResponse.getUnidadeNome());
                        preferenceHelper.putUnidadeCnes(userResponse.getUnidadeCnes());
                        preferenceHelper.putToken(userResponse.getToken());

                        Toast.makeText(Login.this, "Logado com sucesso!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(Login.this, MainActivity.class);
                        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        Login.this.finish();

                    } else {
                        Toast.makeText(Login.this,"Por favor, entrar em contato conosco",Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(Login.this,"Usuário ou senha inválido",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                Toast.makeText(Login.this,"Usuário ou senha inválido",Toast.LENGTH_LONG).show();
            }
        });

    }

}
