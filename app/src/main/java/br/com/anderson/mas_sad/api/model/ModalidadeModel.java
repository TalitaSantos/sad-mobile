package br.com.anderson.mas_sad.api.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ModalidadeModel extends RealmObject {
    @PrimaryKey
    @SerializedName("mod_id")
    @Expose
    private int mod_id;

    @SerializedName("mod_descricao")
    @Expose
    private String mod_descricao;

    @SerializedName("mod_codigo")
    @Expose
    private String mod_codigo;

    public ModalidadeModel() {

    }

    public ModalidadeModel(int mod_id, String mod_descricao, String mod_codigo) {
        this.mod_id = mod_id;
        this.mod_descricao = mod_descricao;
        this.mod_codigo = mod_codigo;
    }

    public int getMod_id() {
        return mod_id;
    }

    public void setMod_id(int mod_id) {
        this.mod_id = mod_id;
    }

    public String getMod_descricao() {
        return mod_descricao;
    }

    public void setMod_descricao(String mod_descricao) {
        this.mod_descricao = mod_descricao;
    }

    public String getMod_codigo() {
        return mod_codigo;
    }

    public void setMod_codigo(String mod_codigo) {
        this.mod_codigo = mod_codigo;
    }

    @NonNull
    @Override
    public String toString() {
        return mod_descricao;
    }
}
