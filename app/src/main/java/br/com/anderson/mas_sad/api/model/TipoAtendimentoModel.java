package br.com.anderson.mas_sad.api.model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TipoAtendimentoModel extends RealmObject {
    @PrimaryKey
    @SerializedName("tat_id")
    @Expose
    private int tat_id;

    @SerializedName("tat_nome")
    @Expose
    private String tat_nome;

    @SerializedName("tat_codigo")
    @Expose
    private String tat_codigo;

    public TipoAtendimentoModel() {

    }

    public TipoAtendimentoModel(int tat_id, String tat_nome, String tat_codigo) {
        this.tat_id = tat_id;
        this.tat_nome = tat_nome;
        this.tat_codigo = tat_codigo;
    }

    public int getTat_id() {
        return tat_id;
    }

    public void setTat_id(int tat_id) {
        this.tat_id = tat_id;
    }

    public String getTat_nome() {
        return tat_nome;
    }

    public void setTat_nome(String tat_nome) {
        this.tat_nome = tat_nome;
    }

    public String getTat_codigo() {
        return tat_codigo;
    }

    public void setTat_codigo(String tat_codigo) {
        this.tat_codigo = tat_codigo;
    }

    @NonNull
    @Override
    public String toString() {
        return tat_nome;
    }
}
