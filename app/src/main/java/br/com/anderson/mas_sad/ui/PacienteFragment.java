package br.com.anderson.mas_sad.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.List;
import br.com.anderson.mas_sad.ListViewAdapter;
import br.com.anderson.mas_sad.R;
import br.com.anderson.mas_sad.api.model.PacienteModel;
import io.realm.Realm;
import io.realm.RealmResults;

public class PacienteFragment extends Fragment {

    private ListViewAdapter adapter;
    private ListView mListView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_paciente, container, false);

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<PacienteModel> pacientes = realm.where(PacienteModel.class).findAll();

        final List<PacienteModel> pList = new ArrayList<>();
        for (int i = 0; i < pacientes.size(); i++) {
            PacienteModel t = new PacienteModel(
                    pacientes.get(i).getPac_id(),
                    pacientes.get(i).getPac_nome(),
                    pacientes.get(i).getPac_cpf(),
                    pacientes.get(i).getPac_endereco(),
                    pacientes.get(i).getPac_cep(),
                    pacientes.get(i).getPac_email(),
                    pacientes.get(i).getPac_telefone(),
                    pacientes.get(i).getPac_cns(),
                    pacientes.get(i).getPac_data_nascimento(),
                    pacientes.get(i).getSexo(),
                    pacientes.get(i).getEquipe(),
                    pacientes.get(i).getSex_id(),
                    pacientes.get(i).getEqu_id()
            );
            pList.add(t);
        }

        populateListView(view, pList);

        realm.commitTransaction();
        realm.close();

        return view;
    }

    private void populateListView(View v, List<PacienteModel> pacienteModelList) {
        mListView = v.findViewById(R.id.listViewId);
        adapter = new ListViewAdapter(this.getActivity(), pacienteModelList);
        mListView.setAdapter(adapter);
    }

    public static PacienteFragment newInstance() {
        return new PacienteFragment();
    }
}