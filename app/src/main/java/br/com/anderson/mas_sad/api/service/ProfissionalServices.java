package br.com.anderson.mas_sad.api.service;

import java.util.List;
import br.com.anderson.mas_sad.api.model.ProfissionalModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface ProfissionalServices {
    @GET("profissional/{equipe}")
    Call<List<ProfissionalModel>> listProfissionais(
            @Path("equipe") String profissional,
            @Header("x-access-token") String token
    );
}
